package org.example.model;

public class SecondDto extends BaseDto {

    private final String name;
    private final Integer number;
    private final Character character;
    private final Float floatNumber;

    public SecondDto(long id, String name, Integer number, Character character, Float floatNumber) {
        super(id);
        this.name = name;
        this.number = number;
        this.character = character;
        this.floatNumber = floatNumber;
    }
}
