package org.example.model;

public class FirstDto extends BaseDto {

    private String name;
    private int number;
    private SecondDto secondExample;

    public FirstDto(long id, String name, int number, SecondDto secondExample) {
        super(id);
        this.name = name;
        this.number = number;
        this.secondExample = secondExample;
    }
}