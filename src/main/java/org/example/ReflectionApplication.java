package org.example;

import org.example.model.FirstDto;
import org.example.model.SecondDto;

import java.util.List;

public class ReflectionApplication {

    public static void main(String[] args) {
        var testCollection = getTestCollection();
    }

    private static List<Object> getTestCollection() {
        var example_1 = new FirstDto(1, "Simple name", 11, new SecondDto(3, "Complex name", 111, 'u', 11.1F));
        var example_2 = new FirstDto(2, "New name", 22, new SecondDto(5, "Old name", 222, 'g', 22.2F));
        var example_3 = new FirstDto(3, "Weird name", 33, new SecondDto(8, "Normal name", 333, 'k', 33.3F));
        return List.of(example_1, example_2, example_3);
    }
}

